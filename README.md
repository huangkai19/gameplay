# gameplay

游戏开发相关实践，文档记录

记录基于前端技术的游戏开发实践，记录和总结游戏开发中设计的相关知识

基本上是基于HTML，Javascript（ES5,ES6,Typescript），CSS3技术

计划使用的框架有 Vue2.x， Vue3，react，electron 等。

希望通过实践，对以往的知识温故知新，对现有新的开发方式进行学习实践。

每开发一个游戏，都会对 游戏设计 和 代码设计 过程进行梳理

适当地收集游戏开发相关的素材，一般都是 音频 和 图片